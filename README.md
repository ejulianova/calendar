## Features

* locale: en-GB
* displays current month (first day of week depends on the locale)
* highlights current date
* allows navigating to previous/next month
* allows adding reminders
* displays added reminders chronologically

## TODO

* allow editing a reminder
* allow deleting a reminder
* add css tooltips for reminders
* preserve reminders in localStorage
* send browser notification when it's time for a reminder
* write tests :}
* i18n - allow changing the locale
* [refactor] extract styles for each component in separate file
* [refactor] extract colors, fonts, spacings variables in separate scss file
* [refactor] extract shared styles in mixins to make them reusable
* [refactpr] rethink calendar mobile view
* [refactor] consider adding some state management
* [refactor] use css modules (unique css class names)
* [refactor] extract all strings in a resource file (allow support for translations)

## Set up

Follow these steps to set up the calendar:
* `git clone https://github.com/ejulianova/calendar.git`
* `cd calendar`
* `npm install`

## Scripts

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.<br>

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

### `npm run eject`
