import React, { useState } from 'react';

import Month from './Month';
import Navbar from './NavBar';
import ReminderForm from './ReminderForm';

import './Calendar.scss';

const Calendar = () => {
  const [date, setDate] = useState(new Date());
  const [reminders, setReminders] = useState([]);
  const addReminder = (reminder) => {
    reminders.push(reminder);
    reminders.sort();
    setReminders([...reminders]);
  };

  return (
    <div className="calendar">
      <Navbar date={date} onChange={setDate} />
      <Month date={date} reminders={reminders} />
      <ReminderForm onAdd={addReminder}/>
    </div>
  );
};

export default Calendar;
