import React, { useState } from 'react';
import { format, addMonths, subMonths } from 'date-fns';

function Navbar (props) {
  const [date, setDate] = useState(props.date);
  const prevMonth = () => {
    const newDate = subMonths(date, 1)
    setDate(newDate);
    props.onChange(newDate);
  };
  const nextMonth = () => {
    const newDate = addMonths(date, 1);
    setDate(newDate);
    props.onChange(newDate);
  };

  return (
    <nav className='calendar-navbar'>
      <button title="Previous Month" onClick={prevMonth}>
        <i className="arrow-left"></i>
      </button>
      <span className='calendar-title'>{format(date, 'MMMM, y')}</span>
      <button title="Next Month" onClick={nextMonth}>
        <i className="arrow-right"></i>
      </button>
    </nav>
  );
}

export default Navbar;
