import React, { useState } from 'react';
import { format } from 'date-fns';

function Form ({ onAdd, onCancel }) {
  const now = new Date();
  const [title, setTitle] = useState('');
  const [date, setDate] = useState(format(now, 'yyyy-MM-dd'));
  const [time, setTime] = useState(format(now, 'hh:mm'));
  const onSubmitClick = () => {
    const [year, month, day] = date.split(/\D/);
    const [hours, minutes] = time.split(/\D/);
    const datetime = new Date(year, month - 1, day, hours, minutes);

    onAdd({
      title,
      datetime,
    });
  };
  const onCancelClick = () => {
    onCancel();
  };

  return (
    <form className="calendar-form">
      <h1>Add a reminder:</h1>
      <fieldset>
        <label htmlFor="title">Title</label>
        <input autoFocus name="title" value={title} onChange={e => setTitle(e.target.value)} />
      </fieldset>
      <fieldset>
        <label htmlFor="date">Date</label>
        <input type="date" value={date} min={date} name="date" onChange={e => setDate(e.target.value)} />
      </fieldset>
      <fieldset>
        <label htmlFor="time">Time</label>
        <input type="time" value={time} name="time" onChange={e => setTime(e.target.value)} />
      </fieldset>
      <button onClick={onSubmitClick} className='calendar-button is-submit'>Submit</button>
      <button onClick={onCancelClick} className='calendar-button'>Cancel</button>
    </form>
  );
}

function ReminderForm ({ onAdd }) {
  const [visible, setVisible] = useState(false);
  const onReminderAdd = (reminder) => {
    onAdd(reminder);
    setVisible(false);
  }
  return (
    <div>
      {visible && <Form onAdd={onReminderAdd} onCancel={() => setVisible(false)} />}
      <button className="calendar-add" onClick={() => setVisible(true)}>+</button>
    </div>
  );
}

export default ReminderForm;
