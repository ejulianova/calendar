import React from 'react';
import {
  format,
  endOfWeek,
  startOfWeek,
  endOfMonth,
  startOfMonth,
  eachDayOfInterval,
  isSameDay
} from 'date-fns';

import Day from './Day';
import DayOfWeek from './DayOfWeek';

const getWeekDayNames = () => {
  const now = new Date();
  const days = eachDayOfInterval({ start: startOfWeek(now), end: endOfWeek(now) });
  return days.map(day => format(day, 'iii'));
}

const getMonthBounds = (date = new Date()) => {
  const firstDayOfMonth = startOfMonth(date);
  const lastDayOfMonth = endOfMonth(date);
  return {
    start: startOfWeek(firstDayOfMonth),
    end: endOfWeek(lastDayOfMonth),
  }
};

function Month ({ date, reminders }) {
  const { start, end } = getMonthBounds(date);
  const daysOfWeek = getWeekDayNames()
    .map(name => <DayOfWeek key={name} title={name} />);
  const days = eachDayOfInterval({ start, end })
    .map((day) => {
      const remindersForCurrentDay = reminders.filter(reminder => isSameDay(day, reminder.datetime));
      return (
        <Day
          date={day}
          key={day.getTime()}
          selectedDate={date}
          reminders={remindersForCurrentDay}
        />
      );
    });

  return <div className="calendar-month">{daysOfWeek}{days}</div>;
}

export default Month;
