import React from 'react';

function DayOfWeek ({ title }) {
  return <div className="calendar-day is-title">{title}</div>
}

export default DayOfWeek;
