import React from 'react';
import { isToday, isSameDay, isWeekend } from 'date-fns';

import Reminder from './Reminder';

function Day ({ date, selectedDate, reminders }) {
  const classNames = ['calendar-day'];
  const remindersList = reminders.map(({title, datetime}, i) =>
    <Reminder title={title} datetime={datetime} key={`${i}-${datetime.getTime()}`} />);

  if (isWeekend(date)) {
    classNames.push('is-weekend');
  }

  if (date.getMonth() !== selectedDate.getMonth()) {
    classNames.push('is-faded');
  }

  if (isSameDay(date, selectedDate)) {
    classNames.push(isToday(date) ? 'is-current' : 'is-selected');
  }

  return (
    <div className={classNames.join(' ')}>
      {date.getDate()}
      {remindersList}
    </div>
  );
}

export default Day;
