import React from 'react';

function Reminder ({ title, datetime }) {
  const tooltip = `${title} at ${datetime.toLocaleTimeString()}`;
  return (
    <span className="calendar-reminder" title={tooltip}>{ title }</span>
  );
}

export default Reminder;
